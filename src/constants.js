export const COLORS = {
  white: 'hsl(0deg 0% 100%)',
  gray: {
    100: '185deg 5% 95%',
    300: '190deg 5% 80%',
    500: '196deg 4% 60%',
    700: '220deg 5% 40%',
    900: '220deg 3% 20%',
  },
  primary: '340deg 65% 47%',
  secondary: '240deg 60% 63%',
};

export const WEIGHTS = {
  normal: 500,
  medium: 600,
  bold: 800,
};

 const BREAKPOINTS ={
  // breakpoints:
  // | Name   | Width (px) | Width (rem) |
  // | ------ | ---------- | ----------- |
  // | phone  | 600        | 37.5        |
  // | tablet | 950        | 59.375      |
  // | laptop | 1300       | 81.25       |

  phoneMax: 37.5,
  tabletInner: 59.37,
  laptopMin: 81.25
}; 


export const QUERIES = {
  'testMW': `(max-width: 1000px)`,
  'tabletAndDown': ` (max-width: ${BREAKPOINTS.laptopMin}rem)`,
  'phoneAndDown': ` (max-width: ${BREAKPOINTS.phoneMax}rem)`,
}
