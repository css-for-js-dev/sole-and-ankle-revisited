import React from 'react';
import styled from 'styled-components/macro';

import { COLORS, WEIGHTS, QUERIES } from '../../constants';
import Logo from '../Logo';
import SuperHeader from '../SuperHeader';
import MobileMenu from '../MobileMenu';
import SearchInput from '../SearchInput';
import UnstyledButton from '../UnstyledButton';
import Icon from '../Icon';

const Colors = {
 '--color-gray-300' : COLORS.gray[300],
  '--color-gray-900' : COLORS.gray[900],
  '--color-secondary' : COLORS.secondary
};

const Header = () => {
  const [showMobileMenu, setShowMobileMenu] = React.useState(false);

  // For our mobile hamburger menu, we'll want to use a button
  // with an onClick handler, something like this:
  //
  // <button onClick={() => setShowMobileMenu(true)}>

  return (
    <header style = { Colors }>
      <SuperHeader />
      <MainHeader>
        <Side>
          <Logo />
        </Side>
        <Nav>
          <NavLink href="/sale">Sale</NavLink>
          <NavLink href="/new">New&nbsp;Releases</NavLink>
          <NavLink href="/men">Men</NavLink>
          <NavLink href="/women">Women</NavLink>
          <NavLink href="/kids">Kids</NavLink>
          <NavLink href="/collections">Collections</NavLink>

        </Nav>
        <Side/>
        <SideRight >
          <Icon id="shopping-bag" strokeWidth={1} />
          <Icon id="search" strokeWidth={1} />
          <UnstyledButton onClick={() => setShowMobileMenu(true)}>
            <Icon id="menu" strokeWidth={1} />
          </UnstyledButton>
        </SideRight>
      </MainHeader>

      <MobileMenu
        isOpen={showMobileMenu}
        onDismiss={() => setShowMobileMenu(false)}
      />
    </header>
  );
};

const MainHeader = styled.div`
  display: flex;
  align-items: baseline;
  padding: 18px 32px;
  border-bottom: 1px solid var(--color-gray-300);
   overflow-x: auto;
`;

const Nav = styled.nav`
  display: flex;
  gap: clamp( 
        1rem,
        12vw - 7.5rem,
        5rem
        );
  margin: 0px 48px;


  @media ${QUERIES.tabletAndDown}{
    display: none;
  }
`;

const Side = styled.div`
  flex: 1;
`;

const SideRight = styled(Side)`
  display: none;

  @media ${QUERIES.tabletAndDown}{
    display: flex;
    justify-content: space-between;
    max-width: 138px;
    }
`

const NavLink = styled.a`
  font-size: 1.125rem;
  text-transform: uppercase;
  text-decoration: none;
  color: var(--color-gray-900);
  font-weight: ${WEIGHTS.medium};

  &:first-of-type {
    color: var(--color-secondary);
  }
`;

export default Header;
