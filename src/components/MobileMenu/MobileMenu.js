/* eslint-disable no-unused-vars */
import React from 'react';
import styled from 'styled-components/macro';
import { DialogOverlay, DialogContent } from '@reach/dialog';

import { COLORS, QUERIES, WEIGHTS } from '../../constants';

import UnstyledButton from '../UnstyledButton';
import Icon from '../Icon';
import VisuallyHidden from '../VisuallyHidden';

const MobileMenu = ({ isOpen, onDismiss }) => {

  return (
    <Wrapper isOpen={isOpen} onDismiss={onDismiss}>
      <Modal>
        <Close onClick={onDismiss}>
            <Icon id="close" strokeWidth={1} />
        </Close>
        <Menu>
          <NavLink href="/sale">Sale</NavLink>
          <NavLink href="/new">New&nbsp;Releases</NavLink>
          <NavLink href="/men">Men</NavLink>
          <NavLink href="/women">Women</NavLink>
          <NavLink href="/kids">Kids</NavLink>
          <NavLink href="/collections">Collections</NavLink>
        </Menu>
        <Footer>
          <Link href="/terms">Terms and Conditions</Link>
          <Link href="/privacy">Privacy Policy</Link>
          <Link href="/contact">Contact Us</Link>
        </Footer>
    </Modal>
    </Wrapper>
  );
};

const Wrapper = styled(DialogOverlay)`
  position: fixed;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;

  font-family: inherit;

  background: ${COLORS.gray[700]};
  background: hsl(220deg 5% 40% / 0.8);
  background: var(--color-backdrop);
`

const Modal = styled(DialogContent)`
  width: 300px;
  height: 100%;
  background: ${COLORS.white};
  position: fixed;
  top: 0;
  bottom: 0;
  right: 0;
  padding-left: 32px;

  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

const Close = styled(UnstyledButton)`
  width: 36px;
  height: 36px;
  margin-top: 14px;
  margin-right: 4px;

  align-self: end;
`;

const Menu = styled.nav`
  display: flex;
  flex-direction: column;
  gap: 22px;
  overflow: auto;
  justify-content: center;
  
  text-decoration: none;
  color: ${COLORS.gray[900]};
  font-size: ${ 18 / 16 }rem;

`

const Link = styled.a`
  text-decoration: inherit;
  color: inherit;
  font-size: inherit;
  font-weight: ${WEIGHTS.medium};
  text-transform: uppercase;
`;

const NavLink = styled(Link)`
  :first-of-type {
    color: ${COLORS.secondary};
    }
`

const Footer = styled.footer`
  display: flex;
  flex-direction: column;
  gap: 14px;
  margin-bottom: 32px;

  font-size: ${14/16}rem;
  color: ${COLORS.gray[700]};
`;

export default MobileMenu;
